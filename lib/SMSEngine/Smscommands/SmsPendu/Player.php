<?php

namespace SMSEngine\SmsCommands\SmsPendu ;

class Player
{
	/**
	 * @var string
	 */
	public $name ;
	/**
	 * @var int
	 */
	public $score ;

	/**
	 * @param string $phone
	 */
	public function __construct( $name )
	{
		$this->name = $name ;
		$this->score = 0 ;
	}
}