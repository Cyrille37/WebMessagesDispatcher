<?php

namespace SMSEngine\SmsCommands\SmsPendu ;

use Log ;

class Game
{
	const SECRET_CHAR = '#' ;
	const STEPS_MAX = 10 ;
	/**
	 * @var Player
	 */
	public $owner ;
	/**
	 * @var Player[]
	 */
	public $guests ;
	/**
	 * @var string
	 */
	public $finalWord ;
	/**
	 * @var string
	 */
	public $word ;
	/**
	 * @var int
	 */
	public $stepsLeft ;

	/**
	 * 
	 * @param string $name
	 * @param string $word
	 * @param int $stepMax
	 */
	public function __construct( $name, $word='girafe', $stepMax = self::STEPS_MAX )
	{
		$this->owner = new Player( $name );
		$this->guests = [] ;

		// Attention !!
		// Ça fonctionnait bien avec php-cli (phpunit) mais pas avec php-fpm
		// La seule solution que j'ai trouvé est d'ajouté
		//	setlocale( LC_CTYPE, env('APP_LOCALE', 'en') );
		// 	dans bootstrap/app.php
		// Et bien entendu de régler la locale qui va bien dans .env => APP_LOCALE=fr_FR.utf8

		// Remove "Accentuated chars" and make the word "Upper case"
		$this->finalWord = strtoupper( iconv( 'UTF-8', 'ASCII//TRANSLIT//IGNORE', $word ) );
		//Log::debug(__METHOD__,['finalWord'=>$this->finalWord, 'word'=>$word , 'this.word'=>$this->word ]);

		$this->stepsLeft = $stepMax ;

		$this->word = str_repeat( self::SECRET_CHAR, strlen($this->finalWord) );

	}

	public function addGuest( $name )
	{
		foreach( $this->guests as $player )
			if( $player->name == $name )
				return false ;
		array_push( $this->guests, new Player($name) );
		return true ;
	}

	/**
	 * @param string $name
	 * @return boolean
	 */
	public function isOwner( $name )
	{
		return $this->owner->name == $name ;
	}

	/**
	 * @return Player[]
	 */
	public function getPlayers()
	{
		return array_merge( [ $this->owner ], $this->guests );
	}

	/**
	 * @return Player
	 */
	public function getPlayer( $playerName )
	{
		foreach( $this->getPlayers() as $player )
		{
			if( $player->name == $playerName )
			{
				return $player ;
			}
		}
		return null;
	}

	public function isFinish()
	{
		return strpos($this->word, self::SECRET_CHAR)===false ? true : false ;
	}

	/**
	 * @return boolean
	 */
	public function hasStepsLeft()
	{
		return $this->stepsLeft > 0 ;
	}

	/**
	 * @param string $playerName
	 * @param string $letter
	 * @return boolean
	 */
	public function play( $playerName, $letter )
	{
		// Remouve "Accentuated chars" and make the letter "Upper case"
		$letter = strtoupper( iconv( 'UTF-8', 'ASCII//TRANSLIT//IGNORE', $letter ) );

		// Letter already found
		if( str_contains( $this->word, $letter ) )
			return false ;

		$pos = 0 ;
		$found = false ;
		while( ($pos = strpos( $this->finalWord, $letter, $pos)) !== false )
		{
			$this->word[$pos] = $letter ;
			$found = true ;
			$pos ++ ;
		}

		if( ! $found )
		{
			$this->stepsLeft -- ;
			return false ;
		}

		foreach( $this->getPlayers() as $player )
		{
			if( $player->name == $playerName )
			{
				$player->score ++ ;
				break ;
			}
		}

		return true ;
	}

}
