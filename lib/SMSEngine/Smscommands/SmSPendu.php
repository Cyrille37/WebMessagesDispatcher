<?php

namespace SMSEngine\SmsCommands ;

use Log;
use Illuminate\Contracts\Cache\Store;
use SMSEngine\SmsEngine ;
use SMSEngine\Models\Sms;
use SMSEngine\SmsCommand ;
use SMSEngine\SmsCommands\SmsPendu\Game ;
use SMSEngine\SmsCommands\SmsPendu\Player ;

class SmsPendu implements SmsCommand
{
	public $messages =
	[
		'nogame' => "Pas de partie de pendu en cours ;-)\nPour jouer répondez: #pendu",
		'alreaygame' => 'Déjà une partie de pendu en cours ;-)',
		'invalidphonenumber' => 'Numéro de tél invalide :-[',
		'yourinvited' => 'Le %s vous invite à une partie de pendu. Pour jouer répondez: #pendu',
		'alreadyPlayer' => 'Le %s est déjà dans la partie',
		'playresult' => "%s\nReste %d erreurs possibles.\nPour jouer répondre: #pendu suivi d'une lettre",
		'gameWon' => "%s\nC'est gagné !\n :-P",
		'gameLost' => "Perdu! Pendu!\nLe mot était %s\n:-(",
		'gameStop' => "Partie abandonnée.\nLe mot était %s",
		'gameFinished' => "La partie est terminée.",
		'dontunderstand' => "Commande inconnue.\n'#pendu' seul pour commencer une partie,\n'#pendu lettre' pour jouer\n'#pendu invite tél' pour inviter un joueur"
	];

	const GAME_TTL_MINUTES = 30 ;
	/**
	 * pour associé un joueur à une partie (une seule au même moment)
	 * - <from phone>/<game id>
	 * @var string
	 */
	const KEY_PREFIX_PLAYERS = 'wmd/sms/pendu/players/' ;
	/**
	 * pour stocker les parties
	 * - <game id>/<game data>
	 * @var string
	 */
	const KEY_PREFIX_GAMES = 'wmd/sms/pendu/games/' ;
	/**
	 * pour stocker les invitations
	 * - <from phone>/<game id>
	 * @var string
	 */
	const KEY_PREFIX_GUESTS = 'wmd/sms/pendu/guests/' ;

	/**
	 * @var \Illuminate\Contracts\Cache\Store
	 */
	protected $cache ;

	public function __construct( /*Store $cache*/ )
	{
		$this->cache = app('cache');
	}

	/**
	 * 
	 * {@inheritdoc}
	 */
	public function process( $from, $to, $arg )
	{
		/*
		 * #pendu
		 * #pendu A
		 * #pendu invite +336
		 * #pendu stop
		 */
		//Log::debug(__METHOD__.' arg:['.$arg.']');

		$arg = trim($arg);
		// echo "\n",($game==null?'null':'ok'),"\n";

		$smss = [];

		if( $arg == '' )
		{
			//Log::debug(__METHOD__.' Nouvelle partie');
			$smss[] = $this->gameStart( $from );
		}
		else if( preg_match('/^([a-z])$/ui', $arg, $matches) )
		{
			//Log::debug(__METHOD__.' Joue la lettre ['.$matches[1].']');
			$smss = $this->gamePlay( $from, $matches[1] );
		}
		else if( preg_match('/^stop\s*/ui', $arg) )
		{
			//Log::debug(__METHOD__.' Arrête la partie');
			$smss[] = $this->gameStop( $from );
		}
		else if( preg_match('/^invite .*?([+0-9]+)/ui', $arg, $matches) )
		{
			//Log::debug(__METHOD__.' Invite le ['.$matches[1].']');
			$smss[] = $this->gameInvite( $from, $matches[1] );
		}
		else
		{
			//Log::debug(__METHOD__.' Commande inconnue');
			$smss[] = new Sms([
				'to' => $from ,
				'body' => $this->messages['dontunderstand']
			]);
		}

		// echo "\n2.",print_r($smss,true),"\n";
		foreach( $smss as $sms )
		{
			if( empty( $sms->from) )
			{
				$sms->from = $to ;
			}
		}

		return $smss ;
	}

	/**
	 * @param string $from
	 * @return string l'ID du jeu
	 */
	public function getGameId( $from )
	{
		return $this->cache->get( self::KEY_PREFIX_PLAYERS.$from, null );
	}

	/**
	 * @param string $gameId
	 * @return \SMSEngine\SmsCommands\SmsPendu\Game
	 */
	public function getGameById( $gameId )
	{
		return $this->cache->get( self::KEY_PREFIX_GAMES.$gameId, null );
	}

	/**
	 * @param string $from
	 * @return \SMSEngine\SmsCommands\SmsPendu\Game
	 */
	public function getGameByPlayer( $from )
	{
		return $this->cache->get( self::KEY_PREFIX_GAMES.$this->getGameId( $from ), null );
	}

	/**
	 * @param string $from
	 * @return \SMSEngine\Models\Sms
	 */
	public function gameStart( $from, $word = null )
	{
		// He's already a game owner ?
		$game = $this->getGameByPlayer( $from );
		if( $game /*&& $game->isOwner($from)*/ )
		{
			if( ! $game->isFinish() )
				return new Sms([ 'to'=>$from, 'body'=>$this->messages['alreaygame'] ]);
		}
		else
		{
			// Guest replied invitation #14
			$gameId = $this->cache->get( self::KEY_PREFIX_GUESTS.$from );
			$game = $this->getGameById( $gameId );
			if( $game )
			{
				// add to players list
				$this->cache->put( self::KEY_PREFIX_PLAYERS.$from, $gameId, self::GAME_TTL_MINUTES );
				$game->addGuest( $from );
				// Save game state
				$this->cache->put( self::KEY_PREFIX_GAMES.$gameId, $game, self::GAME_TTL_MINUTES );
				// remove invitation
				$this->cache->forget( self::KEY_PREFIX_GUESTS.$from );
				// reply sms to play
				return new Sms([
					'to' => $from ,
					'body' => sprintf( $this->messages['playresult'], $game->word, $game->stepsLeft )
				]);
			}
		}

		// Ok, create a new game

		if( $word == null )
		{
			$word = self::getRandomWord();
		}

		$game = new Game( $from, $word );

		$gameId = sha1( $from.microtime(true).rand(1000,999999) );

		$this->cache->put( self::KEY_PREFIX_GAMES.$gameId, $game, self::GAME_TTL_MINUTES );
		$this->cache->put( self::KEY_PREFIX_PLAYERS.$from, $gameId, self::GAME_TTL_MINUTES );

		return new Sms([
			'to' => $from ,
			'body' => sprintf( $this->messages['playresult'], $game->word, $game->stepsLeft )
		]);

	}

	/**
	 * 
	 * @param unknown $from
	 * @param unknown $letter
	 * @return Sms[]
	 */
	public function gamePlay( $from, $letter )
	{
		$gameId = $this->getGameId( $from );

		if( ! $gameId )
			return [
				new Sms([
					'to'=>$from,
					'body'=> $this->messages['nogame']
				])
			];

		$game = $this->getGameById( $gameId );
		if( ! $game )
			return [
				new Sms([
						'to'=>$from,
						'body'=> $this->messages['nogame']
				])
			];

		$gameFinished = false ;

		if( ! $game->hasStepsLeft() || $game->isFinish() )
		{
			return [
				new Sms([
					'to'=> $from,
					'body'=>$this->messages['gameFinished']
				])
			] ;
		}

		if( ! $gameFinished )
		{
			// Play!
			$game->play( $from, $letter );
			// Save game state
			$this->cache->put( self::KEY_PREFIX_GAMES.$gameId, $game, self::GAME_TTL_MINUTES );
			// Continue or game finished ?

			if( $game->hasStepsLeft() )
			{
				if( $game->isFinish() )
				{
					$body = sprintf( $this->messages['gameWon'], $game->finalWord );
					$gameFinished = true ;
				}
				else
				{
					// Reply players
					$body = sprintf( $this->messages['playresult'], $game->word, $game->stepsLeft );
				}
			}
			else
			{
				$body = sprintf( $this->messages['gameLost'], $game->finalWord );
				$gameFinished = true ;
			}

		}

		if( $gameFinished )
		{
			$score = '' ;
			foreach( $game->getPlayers() as $player )
			{
				if( $score != '')
					$score.=', ';
				$score.= $player->name.': '.$player->score;
			}
			$body.="\nScore: ".$score ;
		}

		// Return a sms for each player

		$smss = [];
		foreach( $game->getPlayers() as $player )
		{
			array_push( $smss, new Sms([ 'to'=>$player->name, 'body'=>$body ]) );
		}
		return $smss ;

	}

	/**
	 * 
	 * @param string $from
	 * @param string $to
	 * @return \SMSEngine\Models\Sms
	 */
	public function gameInvite( $from, $to )
	{
		// tél destination valide
		if( null == ($to = SMSEngine::normalizePhonenumber( $to ) ) )
		{
			return new Sms([
				'to'=> $from,
				'body'=>$this->messages['invalidphonenumber']
			]);
		}

		// Retrouve la partie
		$gameId = $this->getGameId( $from );
		$game = $this->getGameById( $gameId );

		if( $gameId == null || $game == null )
		{
			return new Sms([ 'to'=>$from, 'body'=>$this->messages['nogame'] ]);
		}

		// Partie terminée ?
		if( ! $game->hasStepsLeft() || $game->isFinish() )
		{
			return new Sms([ 'to'=>$from, 'body'=>$this->messages['gameFinished'] ]);
		}

		// invité déjà dans la partie
		if( $this->cache->get( self::KEY_PREFIX_PLAYERS.$to ) != null )
		{
			return new Sms([
				'to'=> $from,
				'body'=> sprintf( $this->messages['alreadyPlayer'], $to)
			]);
		}

		$this->cache->put( self::KEY_PREFIX_GUESTS.$to, $gameId, self::GAME_TTL_MINUTES );

		return new Sms([
			'to'=>$to,
			'body'=> sprintf( $this->messages['yourinvited'],$from )
		] );
	}

	public function gameStop( $from )
	{
		$gameId = $this->getGameId( $from );
		if( $gameId == null )
		{
			return new Sms([ 'to'=>$from, 'body'=>$this->messages['nogame'] ]);
		}
		$game = $this->getGameById( $gameId );

		$this->cache->forget( self::KEY_PREFIX_GAMES.$gameId  );
		$this->cache->forget( self::KEY_PREFIX_PLAYERS.$from );

		return new Sms([ 'to'=>$from, 'body'=> sprintf($this->messages['gameStop'],$game->finalWord) ]);
	}

	public static function getWordsFilesFolder()
	{
		return __DIR__.'/SmsPendu/mots' ;
	}

	public static function getWordsFiles()
	{
		return [
			'httpwwwlistedemotscomlisteanimaux.txt',
			'httpwwwlistedemotscomlistearbres.txt',
			'httpwwwlistedemotscomlistecapitales.txt',
			'httpwwwlistedemotscomlistecouleurs.txt',
			'httpwwwlistedemotscomlisteepices.txt',
			'httpwwwlistedemotscomlistefleurs.txt',
			'httpwwwlistedemotscomlistefruits.txt',
			'httpwwwlistedemotscomlistelegumes.txt',
			'httpwwwlistedemotscomlistemetiers.txt',
			'httpwwwlistedemotscomlistemoyenstransport.txt',
			'httpwwwlistedemotscomlisteoutils.txt',
			'httpwwwlistedemotscomlistepays.txt',
			'httpwwwlistedemotscomlistepokemon.txt',
			'httpwwwlistedemotscomlistesports.txt',
			'httpwwwlistedemotscomlisteverbes1ergroupe.txt',
			'httpwwwlistedemotscomlisteverbes2emegroupe.txt',
			'httpwwwlistedemotscomlisteverbes3emegroupe.txt'
		];
	}

	public static function getRandomWord( $minLength = 4 )
	{
		$folder = self::getWordsFilesFolder() ;
		$files = self:: getWordsFiles();

		$file = $files[ rand(0,count($files)-1) ] ;
		$words = file(
			$folder.'/'.$file,
			FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES
		);
		return $words[ rand(0,count($words)-1) ];
	}

}
