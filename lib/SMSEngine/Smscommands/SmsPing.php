<?php

namespace SMSEngine\SmsCommands ;

use SMSEngine\Models\Sms;
use SMSEngine\SmsCommand ;

class SmsPing implements SmsCommand
{
	/**
	 * 
	 * {@inheritdoc}
	 */
	public function process( $from, $to, $args )
	{
		$ping = self::dnsping();
		$web = self::webping();

		return [
			new Sms([
				'from' => $to,
				'to' => $from,
				'body' => '#Pong '
					.( empty($args) ? '' : "\n".'args: '.$args )
					."\n".'DNS: '.($ping?'Ok':'Failed')
					."\n".'WEB: '.($web?'Ok':'Failed')
			])
		];
	}

	public static function webping( $domain = 'www.google.com', $timeout = 2 )
	{
		$fp = fsockopen('tcp://' . $domain, 80, $errno, $errstr, $timeout);
		if (!$fp || !is_resource($fp))
			return false ; // $errno;
		socket_set_timeout($fp, $timeout);
		fwrite($fp, 'GET /'."\n");

		$r = fread( $fp, 8192 );
		//var_dump ( $r );
		fclose($fp);

		return strlen($r) > 0 ? true : false ;
	}

	public static function dnsping( $dns = '8.8.8.8', $timeout = 2)
	{
		$domain = 'www.google.com.';

		$data = pack('n6', rand(10, 77), 0x0100, 1, 0, 0, 0);
		foreach( explode('.', $domain) as $bit )
		{
			$l = strlen($bit);
			$data .= chr($l) . $bit;
		}
		
		$data .= pack('n2', 2, 1);  // QTYPE=NS, QCLASS=IN
		
		$errno = $errstr = 0;
		$fp = fsockopen('udp://' . $dns, 53, $errno, $errstr, $timeout);
		if (!$fp || !is_resource($fp))
			return false ; // $errno;

		socket_set_timeout($fp, $timeout);
		fwrite($fp, $data);

		$r = fread($fp, 8192);
		//var_dump ( $r );
		fclose($fp);

		return strlen($r) > 0 ? true : false ;
	}
}
