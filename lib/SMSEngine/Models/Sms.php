<?php

namespace SMSEngine\Models;

use \Illuminate\Database\Eloquent\Model;
use \Esensi\Model\Contracts\ValidatingModelInterface;
use \Esensi\Model\Traits\ValidatingModelTrait;

use Carbon\Carbon;

/**
 * 
 * @property int id
 * @property string from
 * @property string to
 * @property string body
 * @property string sent
 * @property string created_at
 * @property string updated_at
 */
class Sms extends Model implements ValidatingModelInterface
{
	/**
	 * https://github.com/esensi/model#validating-model-trait
	 */
	use ValidatingModelTrait ;

	/**
	 * @var array
	 */
	protected $rules = [
		'from' => ['required','min:1','max:255'],
		'to' => ['required','min:1'],
		'body' => [],
		'sent' =>[]
	];

	/**
	 * Permit mass assignement with those fields.
	 * Avoid Illuminate\Database\Eloquent\MassAssignmentException.
	 *
	 * @var array
	 */
	protected $fillable = [
		'from',
		'to',
		'body',
		'sent'
	];
}
