<?php

namespace SMSEngine ;

interface SmsCommand
{
	/**
	 * 
	 * @param string $from
	 * @param string $to
	 * @param string $args
	 * @return \SMSEngine\Models\Sms[] empty array if no answer, one or more sms if replying.
	 */
	public function process( $from, $to, $args );
}
