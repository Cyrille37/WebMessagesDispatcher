<?php

namespace SMSEngine ;

use SMSEngine\Models\Sms ;
use Log ;
use Watson\Validating\ValidationException;

class SmsEngine
{
	const SQL_TABLE_SMS = 'sms' ;
	const DEFAULT_INTER_INDIC = '+33' ;

	protected $commands = [] ;

	public function __construct()
	{
		$this->commands['ping'] = '\SMSEngine\SmsCommands\SmsPing';
		$this->commands['pendu'] = '\SMSEngine\SmsCommands\SmsPendu';
	}
	
	/**
	 * 
	 * eg. unicode body with a smiley:
	 * 	[body] => #ping 😊
	 * 
	 * @param string $from
	 * @param string $from_at
	 * @param string $to
	 * @param string $to_at
	 * @param string $body
	 * @return boolean
	 * @throws Watson\Validating\ValidationException
	 */
	public function process( $from, $from_at, $to, $to_at, $body )
	{
		//Log::debug( __METHOD__ );

		//echo "\n",'1. from:[',$from,'], to['.$to,']',"\n";
		$from = self::normalizePhonenumber( $from );
		$to = self::normalizePhonenumber( $to );
		//echo "\n",'2. from:[',$from,'], to['.$to,']',"\n";

		// If the sms is not a "command" like "#ping something"
		if( ! preg_match('/^#([a-z0-9]+)(.*)$/iums', $body, $matches ) )
		{
			// Do nothing
			return false ;
		}
		$cmd = strtolower($matches[1]);
		$args = isset($matches[2]) ? ltrim($matches[2]) : '' ;

		if( ! isset($this->commands[$cmd]) )
			return false ;

		//Log::debug( __METHOD__.' will run cmd:['.$cmd.']' );

		/**
		 * 
		 * @var \SMSEngine\SmsCommand $cmdObj
		 */
		$cmdObj = new $this->commands[$cmd]();
		$smss = $cmdObj->process( $from, $to, $args );

		foreach( $smss as $sms )
		{
			//echo "\n",' from:[',$sms->from,'], to['.$sms->to,'], body:[',$sms->body.']',"\n";
			$sms->saveOrFail();
		}
		return true ;
	}

	public function popSms( $from )
	{
		$sms = Sms
		::where('from','=', $from)
		->where('sent','=', 0)->first();
		if( $sms )
		{
			$sms->sent = true ;
			$sms->save();
		}
		return $sms ;
	}

	public static function normalizePhonenumber( $phone )
	{
		if( preg_match('/^\+[0-9]{11}$/',$phone) )
		{
			return $phone ;
		}
		if( preg_match('/^[0-9]{10}$/',$phone) )
		{
			return self::DEFAULT_INTER_INDIC . substr($phone,1) ;
		}

		return null ;
	}

}
