<?php

namespace WMD\Models;

use \Illuminate\Database\Eloquent\Model;
use \Esensi\Model\Contracts\ValidatingModelInterface;
use \Esensi\Model\Traits\ValidatingModelTrait;
use \Esensi\Model\Traits\SoftDeletingModelTrait;
use \Illuminate\Support\Facades\Validator;
use Prophecy\Argument\Token\IdenticalValueToken;

use Carbon\Carbon;
use Log ;

/**
 * 
 * @property int id
 * @property string service
 * @property string from
 * @property string from_at
 * @property string to
 * @property string to_at
 * @property string body
 * @property boolean dispatched
 * @property string created_at
 * @property string updated_at
 */
class Message extends Model implements ValidatingModelInterface
{
	//use DatePresenter ;

	/**
	 * https://github.com/esensi/model#validating-model-trait
	 */
	use ValidatingModelTrait ;

	/**
	 * @var array
	 */
	protected $rules = [
		'service' => ['required','min:1','max:20'],
		'from' => ['required','min:1','max:255'],
		'from_at' => ['required','min:1'],
		'to' => ['required','min:1','max:255'],
		'to_at' => ['required','min:1'],
		'body' => [],
		'dispatched' => []
	];

	/**
	 * Permit mass assignement with those fields.
	 * Avoid Illuminate\Database\Eloquent\MassAssignmentException.
	 *
	 * @var array
	 */
	protected $fillable = [
		'service',
		'from',
		'from_at',
		'to',
		'to_at',
		'body'
	];

	/**
	 * Trap some Model events
	 * @see http://laravel.com/docs/5.1/eloquent#events
	 */
	protected static function boot()
	{
		parent::boot();

		// Model event : creating
		Message::creating( function ($msg)
		{
			//Log::debug( __METHOD__.' IN from_at: '.var_export($msg->from_at,true) );
			//Log::debug( __METHOD__.' IN to_at: '.var_export($msg->to_at,true) );

			// "SMS" datetime are big timestamp,
			// divide by 1000 then format as DataTime

			if( is_numeric($msg->from_at) )
			{
				if( $msg->from_at > 9999999999 )
					$msg->from_at /= 1000 ;
				$msg->from_at = Carbon::createFromTimeStamp( $msg->from_at ) ;
			}

			if( is_numeric($msg->to_at) )
			{
				if( $msg->to_at > 9999999999 )
					$msg->to_at /= 1000 ;
					$msg->to_at = Carbon::createFromTimeStamp( $msg->to_at ) ;
			}

			//Log::debug( __METHOD__.' OUT from_at: '.var_export($msg->from_at,true) );
			//Log::debug( __METHOD__.' OUT to_at: '.var_export($msg->to_at,true) );

		});
	}

}
