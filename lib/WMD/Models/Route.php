<?php

namespace WMD\Models;

use \Illuminate\Database\Eloquent\Model;
use \Esensi\Model\Contracts\ValidatingModelInterface;
use \Esensi\Model\Traits\ValidatingModelTrait;
use \Esensi\Model\Traits\SoftDeletingModelTrait;
use \Illuminate\Support\Facades\Validator;
use Prophecy\Argument\Token\IdenticalValueToken;

/**
 * @property int id
 * @property string from_service
 * @property string from
 * @property string to_service
 * @property string to_params
 * @property string to
 * @property string comment
 * @property string created_at
 * @property string updated_at
 */
class Route extends Model implements ValidatingModelInterface
{
	//use DatePresenter ;

	/**
	 * https://github.com/esensi/model#validating-model-trait
	 */
	use ValidatingModelTrait ;

	/**
	 * @var array
	 */
	protected $rules = [
		'from_service' => ['required','min:1','max:20'],
		'from' => ['required','min:1','max:255'],
		'to_service' => ['required','min:1','max:20'],
		'to_params' => ['max:255'],
		'to' => ['required','min:1','max:255'],
		'comment' => []
	];

	/**
	 * Permit mass assignement with those fields.
	 * Avoid Illuminate\Database\Eloquent\MassAssignmentException.
	 *
	 * @var array
	 */
	protected $fillable = [
		'from_service',
		'from',
		'to_service',
		'to_params',
		'to',
		'comment'
	];

}
