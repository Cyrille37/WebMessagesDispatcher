<?php

namespace WMD ;

use WMD\Models\Message ;

interface IDispatcher
{
	/**
	 * @return string The dispatcher identifier
	 */
	public function getId();

	/**
	 * @return string The dispatcher human name
	 */
	public function getName();
	
	public function process( Message $msg, $params );
}
