<?php

namespace WMD ;

use Log;
use WMD\Models\Message ;
use Watson\Validating\ValidationException;

class WebMessagesDispatcher {

	/**
	 * @var \WMD\IDispatcher[]
	 */
	protected $dispatchers ;

	/**
	 * @var \WMD\Router
	 */
	protected $router ;

	public function __construct()
	{
		$this->router = new \WMD\Router();
		$this->dispatchers = array();
	}

	public function registerDispatcher( $dispatcherClass )
	{
		$disp = new $dispatcherClass();		
		if( isset($this->dispatchers[$disp->getId()]) )
			throw \RuntimeException('Dispatcher id:['.$disp->getId().'] already registred');
		$this->dispatchers[$disp->getId()] = $disp ;
	}

	/**
	 * 
	 * @param Message $msg
	 * @return boolean Message was dispatched or not
	 */
	public function dispatch_message( Message $msg )
	{
		//Log::debug( __METHOD__.' body:['.$msg->body.']' );

		// 1. Store the message

		$msg->saveOrFail();

		// 2. Dispatch the message to registred modules

		$dispatched = false ;

		foreach( $this->dispatchers as $dispatcher )
		{
			//Log::debug(__METHOD__.' Searching route for service:"'.$msg->service.'", dispatcher:"'.$dispatcher->getId().', from:"'.$msg->from.'", to:"'.$msg->to.'"' );

			$route = $this->router->getRoute($msg->service, $dispatcher->getId(), $msg->from, $msg->to );
			if( $route != null )
			{
				//Log::debug(__METHOD__.' ... Yes, a route match - params: "'.$route->to_params.'"');

				$dispatcher->process( $msg, $route->to_params );
				$dispatched = true ;
			}
		}

		$msg->dispatched = $dispatched ;
		$msg->save(); // no fail, because potentially dispatched

		return $dispatched ;
	}

	public function getRouteServices() {
		return array(
			array( 'id'=>'sms', 'label'=>'SMS' ),
			array( 'id'=>'web', 'label'=>'Web' )
		);
	}

	public function getRouteModules() {
		
		$routeModules = array();
		foreach( $this->dispatchers as $dispatcher )
		{
			$routeModules[] = array(
				'id' => $dispatcher->getId(), 'label' => $dispatcher->getName()
			);
		}
		return $routeModules ;
	}

	public function getRouter()
	{
		return $this->router;
	}

}