<?php

namespace WMD ;

use Log;

class Router {

	public function __construct(){
		
	}

	public function getRoutes()
	{
		return \WMD\Models\Route::all();
	}

	public function getRoutesCount()
	{
		return \WMD\Models\Route::all()->count();
	}

	public function putRoute( $data )
	{
		if( is_array($data) )
			$id = $data['id'];
		else
			$id = $data ;

		$route = \WMD\Models\Route::findOrFail( $id );
		$route->update($data);

		if( ! $route->save() )
		{
			throw new \Exception('Failed to update route');
		}
		return $route ;
	}

	public function postRoute( $data )
	{
		/**
		 * 
		 * @var \WMD\Models\Route
		 */
		$route = \WMD\Models\Route::create( $data );
		$ok = $route->save();
		if( ! $ok){
			throw new \Exception('Failed to create route ('.$route->getErrors() );
		}
		return $route ;
	}

	/**
	 * Delete a route
	 * @param Array| $data
	 */
	public function deleteRoute( $data )
	{
		if( is_array($data) )
			$id = $data['id'];
		else
			$id = $data ;
		$route = \WMD\Models\Route::findOrFail( $id );
		return $route->delete();
	}

	/**
	 * Return first matching route, or null.
	 * 
	 * @param string $from_service
	 * @param string $to_service
	 * @param string $from
	 * @param string $to
	 * @return \WMD\Models\Route|null
	 */
	public function getRoute( $from_service, $to_service, $from, $to )
	{
		$routes = \WMD\Models\Route::all()
			->where('from_service', $from_service)
			->where('to_service',$to_service);

		if( empty($routes) )
			return null ;

		// Parse results several times to organize priority
		// 1. FROM && TO
		// 2. TO && *
		// 3. FROM && *
		// 4. * && *

		// match FROM && TO
		foreach( $routes as $route )
		{
			if( $route->from == $from && $route->to == $to )
				return $route ;
		}
		// match TO && *
		foreach( $routes as $route )
		{
			if( $route->to == $to && $route->from == '*' )
				return $route ;
		}
		// match FROM && *
		foreach( $routes as $route )
		{
			if( $route->from == $from && $route->to == '*' )
				return $route ;
		}
		// match * && *
		foreach( $routes as $route )
		{
			if( $route->from == '*' && $route->to == '*' )
				return $route ;
		}
		return null ;
	}

}
