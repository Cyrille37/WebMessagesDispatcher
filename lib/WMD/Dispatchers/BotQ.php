<?php

namespace WMD\Dispatchers ;

use Log;
use WMD\Models\Message ;

class BotQ implements \WMD\IDispatcher {

	/**
	 * TODO: move to configuration.
	 * @var string The BotQ server url
	 */
	const BOQ_URL = 'http://botq.local.comptoir.net';

	/**
	 * 
	 * {@inheritDoc}
	 * @see \WMD\IDispatcher::getName()
	 */
	public function getName()
	{
		return 'Bot Queue' ;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \WMD\IDispatcher::getId()
	 */
	public function getId()
	{
		return 'botq' ;
	}
	
	public function process( Message $msg, $params )
	{
		$channelId = 1 ;
		$priority= 100 ;

		$url = self::BOQ_URL.'/api/textMessage/'.$channelId.'/'.$priority.'/'.urlencode( $msg->body ) ;
		$hc = new \HttpClient();
		$hc->request($url);
	}

}
