<?php

namespace WMD\Dispatchers ;

use Log;
use DB;
use WMD\Models\Message ;
use WordsCloud\Models\Word;

class WordsCloud implements \WMD\IDispatcher {

	const SQL_TABLE_WORDS = 'swswall_words' ;


	/**
	 *
	 * {@inheritDoc}
	 * @see \WMD\IDispatcher::getName()
	 */
	public function getName()
	{
		return 'Words Cloud' ;
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see \WMD\IDispatcher::getId()
	 */
	public function getId()
	{
		return 'wordscloud' ;
	}

	public function process( Message $msg, $params )
	{
		$this->addWords( $msg->body );
	}

	public static $smileys = array (
			':-)-~' => 'cigarette',
			'0:-)' => 'innocent' ,
			':’(' => 'une larme',
			':-(' => 'déçu',
			':-)' => 'sourire',
			':-*' => 'bisou',
			':-D' => 'mort de rire',
			':-I' => 'indifférence',
			':-o' => 'oh!',
			':-P' => 'tirer la langue',
			':-x' => 'aucun commentaire',
			':-X' => 'bisou',
			';-)' => 'clin d’oeil',
			';->' => 'coquin',
			':*' => 'bisou',
	);

	//public static $dummychars = array('’','\'','(',')','[',']');
	//public static $delimiters = array(',','.',';',':');

	public static $stopWords = array('avec','une','les','des','ses','cette','dans','sur',
			'que','pas','qui','est','par','leur','leurs','même','alors','parce');

	/**
	 * Extract words from message then put them into da db table.
	 */
	public function addWords($text) {

		// search for smileys, replace them by the corresponding word.
		$text = str_replace ( array_keys ( self::$smileys ), array_values ( self::$smileys ), $text );

		// search for dummies chars, remove them.
		$text = preg_replace( '/[^a-zéèçàù]/i', ' ', $text );

		// search for smileys, replace them by the corresponding word.
		$text = str_replace ( array_keys ( self::$smileys ), array_values ( self::$smileys ), $text );

		// slipt words
		$textparts = explode ( ' ', $text );

		// insert each words into db
		foreach($textparts as $word)
		{
			// only words >= 3
			if( mb_strlen($word,'UTF-8') <3)
				continue;
			// do not store stop words
			if( in_array( strtolower($word), self::$stopWords) )
				continue;

			$word = new Word(['word'=>$word]);
			$word->saveOrFail();

		}
	}

}
