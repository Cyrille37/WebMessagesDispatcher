<?php

namespace WMD\Dispatchers ;

use Log;
use SMSEngine\SmsEngine ;
use WMD\Models\Message ;

class SmsDispatcher implements \WMD\IDispatcher
{

	/**
	 * 
	 * {@inheritDoc}
	 * @see \WMD\IDispatcher::getName()
	 */
	public function getName()
	{
		return 'Sms' ;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \WMD\IDispatcher::getId()
	 */
	public function getId()
	{
		return 'sms' ;
	}
	
	public function process( Message $msg, $params )
	{
		$smsEngine = new SmsEngine();
		$smsEngine->process( $msg->from, $msg->from_at, $msg->to, $msg->to_at, $msg->body);
	}

}
