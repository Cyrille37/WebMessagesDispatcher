
@extends('layout')

@section('title', 'WMD Login')

@section('content')

<h1>Identification</h1>

<form action="/auth/login" method="POST">
	<div class="form-group">
		<label for="exampleInputEmail1">Email</label>
		<input type="text" id="email" class="form-control" placeholder="Votre email"/>
	</div>
	<div class="form-group">
		<label for="exampleInputEmail1">Email</label>
		<input type="password" id="password" class="form-control" placeholder="Votre mot de passe"/>
	</div>
	<button type="submit" class="btn btn-default">Submit</button>
</form>

@stop

@section('javascript')

	@parent
	<!-- auth.index beg -->
	<script type="text/javascript">
	$(function()
	{
	});
	</script>
	<!-- auth.index end -->
@stop
