<?php

// Lumen 5.2
//use \Laravel\Lumen\Testing\DatabaseTransactions;
// Lumen 5.1
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TestCase extends Laravel\Lumen\Testing\TestCase
{
	use DatabaseTransactions;

	/**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

    protected function getJson( $url, $data=null )
    {
    	return $this->visit(
    		$url,
    		$data,
    		['Accept' => 'application/json']
    	);
    }

}
