<?php

namespace WMD ;

use WMD\Models\Route ;
use WMD\Router ;

class RouterTest extends \TestCase
{

    /**
     */
    public function testRouteMatch01()
    {
		$router = new Router();

    	$r = new Route( [
    		'from_service' => 'from_test',
    		'from' => '*',
    		'to_service' => 'to_test',
    		'to_params' => 'some params',
    		'to' => '*',
    		'comment' => 'blabla',
    	] );
		$this->assertTrue( $r->save() );

		$route = $router->getRoute( $r->from_service, $r->to_service, '*', '*' );
    	$this->assertNotNull( $route );
    	$route = $router->getRoute( $r->from_service, $r->to_service, 'xyz', $r->to );
    	$this->assertNotNull( $route );
    	$route = $router->getRoute( $r->from_service, $r->to_service, '*', 'xyz ');
    	$this->assertNotNull( $route );

		$route = $router->getRoute( $r->from_service.'xyz', $r->to_service, $r->from, $r->to );
    	$this->assertNull( $route );
    	$route = $router->getRoute( $r->from_service, $r->to_service.'xyz', $r->from, $r->to );
    	$this->assertNull( $route );

    }
}
