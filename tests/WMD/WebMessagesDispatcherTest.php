<?php

use \WMD\WebMessagesDispatcher ;
use \WMD\Models\Message ;
use \WMD\Models\Route ;
use Watson\Validating\ValidationException;

class WebMessagesDispatcherTest extends \TestCase
{
	/**
	 * @var \WMD\WebMessagesDispatcher
	 */
	protected $wmd ;

	/**
	 * @var \WMD\Models\Message
	 */
	protected $validMsg ;
	
	//public function __construct( \WMD\WebMessagesDispatcher $wmd )
	public function __construct()
	{
		$this->wmd = app('\WMD\WebMessagesDispatcher');

		$this->assertNotNull( $this->wmd );

		$this->validMsg = new Message( [
				'service' => 'test',
				'from' => 'from blabla',
				'from_at' => 123,
				'to' => 'to blabla',
				'to_at' => 456,
				'body' => 'body blabla'
		] );
	}

	public function testInvalidMessageFail()
	{

		foreach( ['service', 'from', 'from_at', 'to', 'to_at'] as $k )
		{
			try
			{
				$m = new Message( $this->validMsg->toArray() );
				$m->$k = null ;
				$this->wmd->dispatch_message( $m );
				$this->fail('Should throw a Watson\Validating\ValidationException');
			}
			catch( ValidationException $ex )
			{
				//echo "\n",print_r($ex->getErrors(),true),"\n";
				$this->assertArrayHasKey( $k, $ex->getErrors()->getMessages() );
			}

		}

	}

	public function testNoRouteNoDispatch()
	{
		// Do not ! Not compatible with Illuminate\Foundation\Testing\DatabaseTransactions #7
		//Route::truncate();
		Route::getQuery()->delete();

		$this->assertFalse( $this->wmd->dispatch_message( $this->validMsg ) );
	}
}
