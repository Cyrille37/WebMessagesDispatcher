<?php

namespace WMD\Models ;
use Watson\Validating\ValidationException;
use Carbon\Carbon;

class MessageTest extends \TestCase
{
	protected $validMsg ;

	public function __construct()
	{
		$this->validMsg = new Message( [
				'service' => 'test',
				'from' => 'from blabla',
				'from_at' => 123,
				'to' => 'to blabla',
				'to_at' => 456,
				'body' => 'body blabla'
		] );
	}

	public function testSave()
	{
		$m = new Message( $this->validMsg->toArray() );
		$this->assertEquals( true, $m->save() );
	}
	
	/**
	 * Test Message::fillable corresponds to requirements
	 */
	public function testFillable()
	{
		$m = new Message( $this->validMsg->toArray() );
		foreach( ['service', 'from', 'from_at', 'to', 'to_at', 'body'] as $k )
		{
			$this->assertNotEmpty( $m->$k );
		}
	}

	public function testMaxLengthFields()
	{
		$m = new Message( $this->validMsg->toArray());

		// property too long fail to save()
		// Tanks to esensi/model

		$m->service = 'test test test test test test https://github.com/esensi/model#validating-model-trait' ;

		// assert property is too long
		$this->assertGreaterThan( 20, strlen($m->service) );
		// assert it cannot be saved
		$this->assertEquals(false, $m->save());
	}

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRequiredFields()
    {
    	// Test required fields

    	foreach( ['service', 'from', 'from_at', 'to', 'to_at'] as $k )
    	{
    		try
    		{
    			$m = new Message( $this->validMsg->toArray());
    			$m->$k = null ;
    			$m->saveOrFail();
    			$this->fail('Should throw a Watson\Validating\ValidationException');
    		}
    		catch( \Watson\Validating\ValidationException $ex )
    		{
    			//echo "\n",print_r($ex->getErrors(),true),"\n";
    			$this->assertArrayHasKey( $k, $ex->getErrors()->getMessages() );
    		}

    	}

    }

    /**
     * Because I had, one time, a strange unexplained behavior, I prefer to keep this test.
     * issue #3
     */
	public function testCreatedAtUpdatedAt()
	{
		$m = new Message( $this->validMsg->toArray() );
		$this->assertNull($m->created_at);
		$this->assertNull($m->updated_at);

		$now = Carbon::now();

		$this->assertTrue( $m->save() );

		$this->assertEquals( $m->created_at, $m->updated_at );

		// 1 ou moins de 1 seconde de différence ;-)
		$this->assertTrue( $now->diff( new Carbon($m->created_at))->format('%s') <= 1 );
		$this->assertTrue( $now->diff( new Carbon($m->updated_at))->format('%s') <= 1 );

		// raw db select
		$row = app('db')->select('select * from '.$m->getTable().' where id = ?', [$m->id]);

		$this->assertEquals( $m->created_at, $row[0]->created_at );
		$this->assertEquals( $m->updated_at, $row[0]->updated_at );
	}

}
