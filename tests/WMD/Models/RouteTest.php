<?php

namespace WMD\Models ;

class RouteTest extends \TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test01()
    {
    	$r = new Route( [
    		'from_service' => 'test',
    		'from' => 'from blabla',
    		'to_service' => 'test',
    		'to_params' => 'some params',
    		'to' => 'to blabla',
    		'comment' => 'blabla',
    	] );

        // Test Route::fillable meets requirements

    	foreach( ['from_service', 'from', 'to_service', 'to_params', 'to', 'comment'] as $k )
    	{
    		$this->assertNotEmpty( $r->$k );
    	}

    	// Save success

    	$this->assertEquals( true, $r->save() );

    	// property too long fail to save()
    	// Tanks to esensi/model

    	$r->from_service = 'test test test test test test https://github.com/esensi/model#validating-model-trait' ;

    	// assert property is too long
    	$this->assertGreaterThan( 20, strlen( $r->from_service) );
    	// assert it cannot be saved
    	$this->assertEquals( false, $r->save() );

    }
}
