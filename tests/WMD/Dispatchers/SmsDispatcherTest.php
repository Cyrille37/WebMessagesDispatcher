<?php

namespace Dispatchers ;

use \WMD\WebMessagesDispatcher ;
use \WMD\Models\Message ;
use \WMD\Models\Route ;

class SmsDispatcherTest extends \TestCase
{

	public function testDispatch(  )
	{
		/**
		 * @var \WMD\WebMessagesDispatcher
		 */
		$wmd = app('\WMD\WebMessagesDispatcher');
		$this->assertNotNull($wmd);

		$r = new Route( [
				'from_service' => 'sms',
				'from' => '*',
				'to_service' => 'sms',
				'to_params' => '',
				'to' => '*',
				'comment' => 'a test route',
		] );
		$this->assertTrue( $r->save() );

		$m = new Message( [
    		'service' => 'sms',
    		'from' => '1234',
    		'from_at' => 123,
    		'to' => '5678',
    		'to_at' => 456,
    		'body' => 'body blabla'
    	] );

		$this->assertTrue( $wmd->dispatch_message( $m ) );

	}

	public function testDispatchHttpController()
	{
		$wmd = app('\WMD\WebMessagesDispatcher');
		$this->assertNotNull($wmd);

		$r = new Route( [
				'from_service' => 'sms',
				'from' => '*',
				'to_service' => 'sms',
				'to_params' => '',
				'to' => '*',
				'comment' => 'a test route',
		] );
		$this->assertTrue( $r->save() );

		$this->post('/api/message', [
    		'service' => 'sms',
    		'from' => '1234',
    		'from_at' => 123,
    		'to' => '5678',
    		'to_at' => 456,
    		'body' => 'body blabla'
    	], ['Accept' => 'application/json'])
		->seeJson([
			'dispatched' => true
		]);

	}
}
