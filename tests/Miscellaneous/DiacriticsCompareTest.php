<?php
/**
 * Comaring string without diacritics (accents) sensibility.
 * 
 */

namespace Miscellaneous ;

class DiacriticsCompareTest extends \TestCase
{
	public function testCompare01()
	{		
		$s1 = 'en été ça va là' ;
		$s2 = 'en ÉTE Ça va Là' ;

		/*
		 * Pas la bonne voie:
		 * 
		echo "\n";
		mb_eregi( '/'.$s2.'/', $s1, $matches );
		echo 'res1:[', var_export( $matches, true),"]\n" ;

		$res = stristr($s1, $s2);
		echo 'res2:[', var_export( $res, true),"]\n" ;

		$res = stristr($s1, $s2);
		echo 'res2:[', var_export( $res, true),"]\n" ;

		$s = mb_convert_encoding ( $s1 ,'ascii' );
		echo 'res3:[', var_export( $s, true),"]\n" ;
		*/

		$s1 = strtolower( iconv( 'UTF-8', 'ASCII//TRANSLIT//IGNORE',$s1) );
		$s2 = strtolower( iconv( 'UTF-8', 'ASCII//TRANSLIT//IGNORE',$s2) );

		$this->assertTrue( $s1===$s2 );

	}

}
