<?php

namespace SMSEngine ;

use SMSEngine\Models\Sms ;

class SmsEngineTest extends \TestCase
{
	protected $url = '/sms/message' ;

    public function testHttpGetSms()
    {
    	$from = '1234' ;
    	$body = 'bla bla bla ;-)' ;

    	$sms = new Sms([
  			'from' => $from,
  			'to' => '5678',
   			'body' => $body
    	]);
    	$this->assertTrue( $sms->save() );

        $this->getJson( $this->url.'/'.$from )
		->seeJson([
			'from'=> $from,
			'to' => $sms->to,
			'body' => $body
		]);        
    }

    public function testHttpUnicodeGetSms()
    {
    	$from = '1234' ;
    	$body = 'En été c\'est ça là © ;-)' ;

    	$sms = new Sms([
  			'from' => $from,
  			'to' => '5678',
   			'body' => $body
    	]);
    	$this->assertTrue( $sms->save() );

       	$this->getJson( $this->url.'/'.$from )
        ->seeJson([
			'from'=> $from,
			'to' => $sms->to,
			'body' => $body
		]);        
    }
    
    public function testSmsEngineCmdNotExists()
    {
    	$smsengine = new SmsEngine();
    	$this->assertFalse(
			$smsengine->process( '123', 123, '123', 123, 'ping' )
    	);
    	$this->assertFalse(
			$smsengine->process( '123', 123, '123', 123, 'Ping' )
    	);
    	$this->assertFalse(
			$smsengine->process( '123', 123, '123', 123, '#AZEAZEAZE' )
    	);
    	$this->assertFalse(
    		$smsengine->process( '123', 123, '123', 123, '#AZEAZEAZE fsdfsdf' )
    	);

    }

}
