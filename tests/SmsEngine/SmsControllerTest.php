<?php

use Symfony\Component\HttpFoundation\Response ;

class SmsControllerTest extends \TestCase
{
	protected $channel = 'EE661A2D' ;
	protected $config = [
		'channels' => ['EE661A2D' => 'FC18CF533AF96071890830D4A8AD7557']
	];
	protected $jsonPush = '{
		"from":"+33632330218",
		"from_at":"1459984786000",
		"to":"15555215554",
		"to_at":"1459984785949",
		"body":"salut"
	}';

	/**
     * A basic test example.
     *
     * @return void
     */
    public function testPushInvalidAction()
    {
        $this->post('/sms/sms2web?action=blabla')
        ->assertResponseStatus( Response::HTTP_BAD_REQUEST );
	}

    public function testPushUnknowChannel()
    {
        $this->post('/sms/sms2web?action=push')
        ->assertResponseStatus( Response::HTTP_UNAUTHORIZED );

        $this->post('/sms/sms2web?action=push&channel=9999999')
        ->assertResponseStatus( Response::HTTP_UNAUTHORIZED );
    }

    public function testPushInvalidSign()
    {
    	$secret = $this->config['channels']['EE661A2D'] ;

    	$data = json_decode( $this->jsonPush );
		$data->sign = sha1( $secret
			. $data->from . $data->from_at
			. $data->to . $data->to_at
			. $data->body
			. 'Argh!' // <= make sign invalid, niark niark ;-)
		);
		$status = $this->call('POST', '/sms/sms2web?action=push&channel='.$this->channel, [], [], [],
    			["Content-Type"=>"application/json; charset=utf-8","Accept"=> "application/json"],
    			json_encode($data) )
    	->getStatusCode();
    	$this->assertEquals( Response::HTTP_UNPROCESSABLE_ENTITY, $status );
    }

    public function testPushValidSign()
    {
    	$secret = $this->config['channels']['EE661A2D'] ;
    
    	$data = json_decode( $this->jsonPush );
    	$data->sign = sha1( $secret
			. $data->from . $data->from_at
			. $data->to . $data->to_at
			. $data->body
		);
    	$status = $this->call('POST', '/sms/sms2web?action=push&channel='.$this->channel, [], [], [],
			["Content-Type"=>"application/json; charset=utf-8","Accept"=> "application/json"],
			json_encode($data) )
			->getStatusCode();
		$this->assertEquals( Response::HTTP_OK, $status );
    }

}
