<?php

namespace SMSEngine\Commands ;

use SMSEngine\SmsEngine;
use SMSEngine\SmsCommands\SmsPendu;
use SMSEngine\SmsCommands\SmsPendu\Game ;
use SMSEngine\SmsCommands\SmsPendu\Player ;
use SMSEngine\Models\Sms;

class SmsPenduTest extends \TestCase
{
	/**
	 * @var string $from
	 * @var string $from_at
	 * @var string $to
	 * @var string $to_at
	 */
	protected $from, $from_at, $to, $to_at ;

	/**
	 * @var \SMSEngine\SmsEngine $se
	 */
	protected $se ;

	public function __construct()
	{
    	$this->se = new SmsEngine();
		$this->from = '1234' ;
		$this->to = '5678' ;
	}

	/**
	 * 
	 */
	public function testWords()
	{
		/*
		 * Stupid test !
		$badWord = false ;
		for( $i=0; $i<100; $i++ )
		{
			$w = SmsPendu::getRandomWord();
			//if( empty($w) )
			//	$badWord = true ;
			if( strlen(trim($w)) < 2 )
				$badWord = true ;
		}
		$this->assertFalse( $badWord );
		*/

		// /home/cyrille/Code/www/WebMessagesDispatcher
		//echo app()->basePath(),"\n";
/*
		$folder = SmsPendu::getWordsFilesFolder();
		foreach( SmsPendu::getWordsFiles() as $filename )
		{
			if( $filename == 'httpwwwlistedemotscomlisteoutils.txt' )
			{
				$words = file(
					$folder.'/'.$filename,
					FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES
				);
				$w = $words[25];
				echo strtoupper( $w),"\n";
				$s = strtolower( iconv( 'UTF-8', 'ASCII//TRANSLIT//IGNORE',$w) );
				echo $s,"\n";
				$s = strtoupper( iconv( 'UTF-8', 'ASCII//TRANSLIT//IGNORE',$w) );
				echo $s,"\n";
			}
			
		}
*/
/*
		$w = 'Clé' ;
		echo $w,"\n";
		$s = iconv( 'UTF-8', 'ASCII//TRANSLIT//IGNORE', $w );
		echo '1. ', $s,"\n";
		$s = strtoupper( iconv( 'UTF-8', 'ASCII//TRANSLIT//IGNORE', $w ) );
		echo '2. ', $s,"\n";
		$sms = new Sms([
			'to'=>'1234','from'=>'1234', 'body'=>$s
		]);
		$sms->saveOrFail();
		$sms = Sms::where('to','=','1234')->first();
		echo '3. ', $sms->body,"\n";
*/
	}

    public function testSmsEngine()
    {
    	$body = '#' ;

    	$this->assertNotNull(
    		$this->se->process( $this->from, $this->from_at, $this->to, $this->to_at, $body)
    	);
    }

    public function testGameSyntaxes()
    {
    	$p = new SmsPendu();
 
    	$arg = '' ;
    	$r = $p->process($this->from, $this->to, $arg);
		$this->assertInstanceOf( \SMSEngine\Models\Sms::class, $r[0] );

    	$arg = 'A' ;
    	$r = $p->process($this->from, $this->to, $arg);
    	$this->assertInstanceOf( \SMSEngine\Models\Sms::class, $r[0] );

    	$arg = 'invite +33' ;
    	$r = $p->process($this->from, $this->to, $arg);
    	$this->assertInstanceOf( \SMSEngine\Models\Sms::class, $r[0] );

    	$arg = 'invite +33632330218' ;
    	$r = $p->process($this->from, $this->to, $arg);
    	$this->assertInstanceOf( \SMSEngine\Models\Sms::class, $r[0] );

    	$arg = 'stop' ;
    	$r = $p->process($this->from, $this->to, $arg);
    	$this->assertInstanceOf( \SMSEngine\Models\Sms::class, $r[0] );

    	$arg = 'stop là tout de suite' ;
    	$r = $p->process($this->from, $this->to, $arg);
    	$this->assertInstanceOf( \SMSEngine\Models\Sms::class, $r[0] );

    	$arg = 'toto' ;
    	$r = $p->process($this->from, $this->to, $arg);
    	$this->assertInstanceOf( \SMSEngine\Models\Sms::class, $r[0] );

    }

    public function testGameStart()
    {
    	$p = new SmsPendu();

    	$sms = $p->gameStart( $this->from );

    	$this->assertInstanceOf(\SMSEngine\Models\Sms::class, $sms);
    	$this->assertEquals( $this->from, $sms->to );

		$gameId = app('cache')->get( SmsPendu::KEY_PREFIX_PLAYERS.$this->from );
		$game = app('cache')->get( SmsPendu::KEY_PREFIX_GAMES.$gameId );
		
		$this->assertInstanceOf(\SMSEngine\SmsCommands\SmsPendu\Game::class, $game);

    	$players = $game->getPlayers();

    	$this->assertEquals(1, count($players));
		$this->assertInstanceOf(\SMSEngine\SmsCommands\SmsPendu\Player::class, $players[0]);
    	$this->assertEquals($this->from, $players[0]->name );
    }

	public function testGameInviteConditions()
	{
		$p = new SmsPendu();

		// Pas de partie commencée

		$sms = $p->gameInvite( $this->from, '+33632330218' );
		//echo "\n",' from:[', $sms->from,'], to:[',$sms->to,'], body:[',$sms->body,']',"\n";
		$this->assertEquals( $this->from, $sms->to );
		$this->assertEquals( $p->messages['nogame'], $sms->body );
	
		// Partie commencée
		$gameId = 1234 ;
		$game = new Game( $this->from, 'bonbon' );
		app('cache')->put( SmsPendu::KEY_PREFIX_GAMES.$gameId, $game, 1 );
		app('cache')->put( SmsPendu::KEY_PREFIX_PLAYERS.$this->from, $gameId, 1 );

		// Numéro invalide

		$sms = $p->gameInvite( $this->from, '+336323' );
		//echo "\n",' from:[', $sms->from,'], to:[',$sms->to,'], body:[',$sms->body,']',"\n";
		$this->assertEquals( $this->from, $sms->to );
		$this->assertEquals( $p->messages['invalidphonenumber'], $sms->body );
	
		// Numéro valide #1
	
		$arg = '+33632330218';
		$sms = $p->gameInvite( $this->from, $arg );
		//echo "\n",' from:[', $sms->from,'], to:[',$sms->to,'], body:[',$sms->body,']',"\n";
		$this->assertEquals( $arg, $sms->to );
		$this->assertEquals( sprintf($p->messages['yourinvited'], $this->from ), $sms->body );
	
		// Numéro valide #2
	
		$arg = '0632330218';
		$sms = $p->gameInvite( $this->from, $arg );
		//echo "\n",' from:[', $sms->from,'], to:[',$sms->to,'], body:[',$sms->body,']',"\n";
		$this->assertEquals( SMSEngine::normalizePhonenumber($arg), $sms->to );
		$this->assertEquals( sprintf($p->messages['yourinvited'], $this->from ), $sms->body );

		// Invité déjà dans la partie

		$to = '+33632330218';
		app('cache')->put( SmsPendu::KEY_PREFIX_PLAYERS.$to, true, 1 );
		$sms = $p->gameInvite( $this->from, $to );
		$this->assertEquals( sprintf($p->messages['alreadyPlayer'], $to ), $sms->body );

	}

	public function testInviteAlreadyPlayer()
	{
		$p = new SmsPendu();
		
		$owner = '+33611111111';
		$guest1 = '+33622222222';

		$sms = $p->gameStart( $owner, 'bonbon' );
		$sms = $p->gameInvite( $owner, $guest1 );
		$sms = $p->gameStart( $guest1 );
		$sms = $p->gameInvite( $owner, $guest1 );
		$this->assertEquals( sprintf($p->messages['alreadyPlayer'], $guest1), $sms->body);

	}
	
	public function testInviteAlreadyPlayerViaSmsEngine()
	{
		$gw = '+33600000000';
		$owner = '+33611111111';
		$guest1_1 = '+33622222222';
		$guest1_2 = '0622222222';

		$se = new SmsEngine();
		$p = new SmsPendu();

    	$se->process( $owner, 123, $gw, 123, '#pendu' );
    	$se->process( $owner, 123, $gw, 123, '#pendu invite '.$guest1_1 );
    	$se->process( $guest1_1, 123, $gw, 123, '#pendu' );
		$se->process( $owner, 123, $gw, 123, '#pendu invite '.$guest1_2 );
		$se->process( $owner, 123, $gw, 123, '#pendu invite '.$owner );

		$smss = Sms::orderBy('id', 'asc')->get();

		/*echo 'SMSS:',count($smss),"\n";
		foreach( $smss as $sms )
		{
			echo $sms->body, "\n";
		}*/

		$this->assertEquals( $guest1_1, $smss[1]->to );
		$this->assertEquals( sprintf($p->messages['yourinvited'], $owner), $smss[1]->body );

		$this->assertEquals( $owner, $smss[3]->to );
		$this->assertEquals( sprintf($p->messages['alreadyPlayer'], $guest1_1), $smss[3]->body );
		$this->assertEquals( $owner, $smss[4]->to );
		$this->assertEquals( sprintf($p->messages['alreadyPlayer'], $owner), $smss[4]->body );
	}

	public function testInviteOwner()
	{
		$p = new SmsPendu();
	
		$owner = '+33611111111';
	
		$sms = $p->gameStart( $owner, 'bonbon' );
		$sms = $p->gameInvite( $owner, $owner );
		$this->assertEquals( sprintf($p->messages['alreadyPlayer'], $owner), $sms->body);
	}

	public function testInviteButNotStarted()
	{
		$p = new SmsPendu();
		
		$owner = '+33611111111';
		$guest1 = '+33622222222';

		$sms = $p->gameStart( $owner, 'bonbon' );
		$sms = $p->gameInvite( $owner, $guest1 );
		$smss = $p->gamePlay( $guest1, 'a' );
		$this->assertEquals( 1, count($smss) );
		$this->assertEquals($p->messages['nogame'], $smss[0]->body);

	}

	public function testGameGuest()
	{
		$p = new SmsPendu();

		$owner = '+33611111111';
		$guest1 = '+33622222222';
		$guest2 = '+33633333333';
		$word = 'Bonbon' ;
		$error_left = Game::STEPS_MAX ;

		// Owner start game

		$sms = $p->gameStart( $owner, $word );
		$this->assertEquals( $owner, $sms->to );
		$gId = $p->getGameId( $owner );
		$g = $p->getGameByPlayer( $owner );
		$this->assertInstanceOf( \SMSEngine\SmsCommands\SmsPendu\Game::class, $g );
		$this->assertEquals( Game::STEPS_MAX, $g->stepsLeft );
		$this->assertEquals( sprintf($p->messages['playresult'], $g->word, $g->stepsLeft ), $sms->body );

		// Owner play and loses 1 error

		$error_left -- ;
		$smss = $p->gamePlay( $owner, 'z' );
		$this->assertEquals( 1, count($smss));
		$this->assertEquals( $owner, $smss[0]->to );
		$this->assertEquals( $error_left , $g->stepsLeft );
		$this->assertEquals( sprintf($p->messages['playresult'], $g->word, $g->stepsLeft ), $smss[0]->body );

		// Owner invite guest1

		$sms = $p->gameInvite( $owner, $guest1 );
		$this->assertEquals( $guest1, $sms->to );
		$this->assertEquals( sprintf($p->messages['yourinvited'], $owner ), $sms->body );

		// Guest 1 join the game

		$sms = $p->gameStart( $guest1 );
		$this->assertEquals( $guest1, $sms->to );
		$this->assertEquals( $gId, $p->getGameId( $guest1 ));
		$g = $p->getGameByPlayer( $guest1 );
		$this->assertInstanceOf( \SMSEngine\SmsCommands\SmsPendu\Game::class, $g );
		$this->assertEquals( 2, count($g->getPlayers()) );
		$this->assertEquals( $error_left , $g->stepsLeft );
		$this->assertEquals( sprintf($p->messages['playresult'], $g->word, $g->stepsLeft ), $sms->body );

		// Guest 1 play a good letter

		$smss = $p->gamePlay( $guest1, 'b' );
		$this->assertEquals( 2, count($smss));
		$this->assertContains( $owner, [$smss[0]->to, $smss[1]->to] );
		$this->assertContains( $guest1, [$smss[0]->to, $smss[1]->to] );
		$this->assertEquals( $error_left , $g->stepsLeft );
		$this->assertEquals( sprintf($p->messages['playresult'], $g->word, $g->stepsLeft ), $smss[0]->body );
		$this->assertEquals( sprintf($p->messages['playresult'], $g->word, $g->stepsLeft ), $smss[1]->body );

		// Owner invite guest2

		$sms = $p->gameInvite( $owner, $guest2 );
		$this->assertEquals( $guest2, $sms->to );
		$this->assertEquals( sprintf($p->messages['yourinvited'], $owner ), $sms->body );

		// Guest 2 join the game

		$sms = $p->gameStart( $guest2 );
		$this->assertEquals( $guest2, $sms->to );
		$this->assertEquals( $gId, $p->getGameId( $guest2 ));
		$g = $p->getGameByPlayer( $guest2 );
		$this->assertInstanceOf( \SMSEngine\SmsCommands\SmsPendu\Game::class, $g );
		$this->assertEquals( 3, count($g->getPlayers()) );
		$this->assertEquals( $error_left, $g->stepsLeft );
		$this->assertEquals( sprintf($p->messages['playresult'], $g->word, $g->stepsLeft ), $sms->body );

		// Owner play a good letter

		$smss = $p->gamePlay( $owner, 'o' );
		$this->assertEquals( 3, count($smss));
		$this->assertContains( $owner, [$smss[0]->to, $smss[1]->to, $smss[2]->to] );
		$this->assertContains( $guest1, [$smss[0]->to, $smss[1]->to, $smss[2]->to] );
		$this->assertContains( $guest2, [$smss[0]->to, $smss[1]->to, $smss[2]->to] );
		$g = $p->getGameByPlayer( $guest2 );
		$this->assertEquals( $error_left, $g->stepsLeft );
		$this->assertEquals( sprintf( $p->messages['playresult'], $g->word, $g->stepsLeft ), $smss[0]->body );
		$this->assertEquals( sprintf( $p->messages['playresult'], $g->word, $g->stepsLeft ), $smss[1]->body );
		$this->assertEquals( sprintf( $p->messages['playresult'], $g->word, $g->stepsLeft ), $smss[2]->body );

		// Guest 1 play and loses 1 error

		$error_left -- ;
		$smss = $p->gamePlay( $guest1, 'y' );
		$this->assertEquals( 3, count($smss));
		$this->assertEquals( $error_left , $g->stepsLeft );
		$this->assertEquals( sprintf($p->messages['playresult'], $g->word, $g->stepsLeft ), $smss[0]->body );

		// Guest 1 play a good letter and the game is won

		$smss = $p->gamePlay( $guest1, 'n' );
		$this->assertEquals( 3, count($smss));
		$this->assertEquals( $error_left , $g->stepsLeft );
		$this->assertTrue( false !== strstr( $smss[0]->body, sprintf($p->messages['gameWon'], $g->finalWord ) ) );
		$this->assertTrue( false !== strstr( $smss[0]->body, $owner.': ') );
		$this->assertTrue( false !== strstr( $smss[0]->body, $guest1.': ') );
		$this->assertTrue( false !== strstr( $smss[0]->body, $guest2.': ') );

		// Guest 2 play but game finished

		$smss = $p->gamePlay( $guest2, 'r' );
		$this->assertEquals( 1, count($smss));
		$this->assertEquals( $p->messages['gameFinished'], $smss[0]->body);

		// After some times the cache has forgotten the game

		app('cache')->forget( SmsPendu::KEY_PREFIX_GAMES.$gId );
		$smss = $p->gamePlay( $guest2, 'r' );
		$this->assertEquals( $p->messages['nogame'], $smss[0]->body );

	}

}
