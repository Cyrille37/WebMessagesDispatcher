<?php

namespace SMSEngine\Commands ;

use SMSEngine\Models\Sms ;
use SMSEngine\SmsEngine;

class SmsPingTest extends \TestCase
{
    public function testSmsEnginePing()
    {
    	$smsengine = new SmsEngine();

    	$from = '+33123456789' ;
    	$to = '+44987654321' ;
    	$body = '#Ping ;-)' ;
    	$from_at = 666 ;
    	$to_at = 666 ;

    	$this->assertTrue(
    		$smsengine->process( $from, $from_at, $to, $to_at, $body )
    	);

    	$sms = Sms::where('from','=', $to)->where('to','=',$from)->first();
    	$this->assertNotNull( $sms );
		$this->assertStringStartsWith( '#Pong', $sms->body );
    }
}
