<?php

use SMSEngine\SmsCommands\SmsPendu\Game;

class GameTest extends \TestCase
{
	/**
	 * One player
	 */
	public function testPlayReturnValue()
	{
		$owner = 'Toto' ;
		$w = 'abc' ;
		$game = new Game( $owner, $w );
		$this->assertTrue( true, $game->play($owner, 'a') );
		$this->assertFalse( $game->play($owner, 'z') );
	}

	/**
	 * One player
	 */
	public function testPlay01()
	{
		$owner = 'Toto' ;
		$w = 'Banane' ;
		$game = new Game( $owner, $w );
		$game->play($owner, 'b');
		$game->play($owner, 'a');
		$game->play($owner, 'n');
		$game->play($owner, 'a');
		$game->play($owner, 'n');
		$game->play($owner, 'e');
		$this->assertEquals( strtoupper($w), strtoupper($game->word) );
		$this->assertEquals( 4, $game->getPlayer($owner)->score );
	}

	/**
	 * 3 players
	 */
	public function testPlay02()
	{
		$owner = 'Toto' ;
		$guest1 = 'Albert';
		$guest2 = 'Marcel';
		$game = new Game( $owner, 'Abracadabra' );
		$game->addGuest($guest1);
		$game->addGuest($guest2);
		$game->play($owner, 'b');
		$game->play($guest1, 'a');
		$game->play($guest2, 'a');
		$game->play($owner, 'z');
		$game->play($guest1, 'c');

		$this->assertEquals( 'AB'.Game::SECRET_CHAR.'ACA'.Game::SECRET_CHAR.'AB'.Game::SECRET_CHAR.'A', strtoupper($game->word) );
		$this->assertEquals( 1, $game->getPlayer($owner)->score );
		$this->assertEquals( 2, $game->getPlayer($guest1)->score );
		$this->assertEquals( 0, $game->getPlayer($guest2)->score );

	}

	/**
	 * With accentuated chars
	 */
	public function testWordWithDiacriticsAndMixedCase()
	{
		$owner = 'Toto' ;

		$game = new Game( $owner, 'NoëlÇaLà' );
		$this->assertEquals( 'NOELCALA', strtoupper($game->finalWord) );

		$game->play($owner, 'n');
		$game->play($owner, 'o');
		$game->play($owner, 'e');
		$game->play($owner, 'l');
		$game->play($owner, 'c');
		$game->play($owner, 'a');

		$this->assertEquals( 'NOELCALA', strtoupper($game->word) );
		$this->assertEquals( 6, $game->getPlayer($owner)->score );

	}

}