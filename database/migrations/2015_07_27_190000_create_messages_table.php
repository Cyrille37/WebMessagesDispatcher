<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * Les smileys: CHARACTER SET = utf8mb4
     * 
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {

        	$table->increments('id');

			$table->string('service',20);

			$table->string('from');
			$table->dateTime('from_at');

			$table->string('to');
			$table->dateTime('to_at');

			$table->text('body');

			$table->timestamps();

            $table->index('service');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('messages');
    }
}
