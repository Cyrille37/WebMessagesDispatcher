<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes', function (Blueprint $table) {

            $table->increments('id');

            $table->string('from_service',20);
			$table->string('from');
			$table->string('to_service',20);
			$table->string('to_params');
			$table->string('to');
			$table->text('comment');

			$table->timestamps();

			$table->index(['from_service','to_service']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('routes');
    }
}
