#!/usr/bin/env php
<?php
error_reporting(E_ALL);

$urls = [
	'http://www.listedemots.com/liste-animaux',
	'http://www.listedemots.com/liste-arbres',
	'http://www.listedemots.com/liste-capitales',
	'http://www.listedemots.com/liste-couleurs',
	'http://www.listedemots.com/liste-epices',
	'http://www.listedemots.com/liste-fleurs',
	'http://www.listedemots.com/liste-fruits',
	'http://www.listedemots.com/liste-legumes',
	'http://www.listedemots.com/liste-metiers',
	'http://www.listedemots.com/liste-moyens-transport',
	'http://www.listedemots.com/liste-outils',
	'http://www.listedemots.com/liste-pays',
	'http://www.listedemots.com/liste-pokemon',
	'http://www.listedemots.com/liste-sports',
	'http://www.listedemots.com/liste-verbes-1er-groupe',
	'http://www.listedemots.com/liste-verbes-2eme-groupe',
	'http://www.listedemots.com/liste-verbes-3eme-groupe'
];

scrap();

function scrap()
{
	global $urls ;
	foreach( $urls as $url )
	{
		$content = file_get_contents( $url );

		// Ne prend que les mots sans espace ni tiret
		preg_match_all( '#<div class="mot">([\w]+)<\/div>#isuU', $content, $matches );

		echo $url,' ',count($matches[1]),"\n";
		file_put_contents( __DIR__.'/'.preg_replace('/[^\w]/', '', $url ).'.txt', implode( "\n", $matches[1] ) );

		// be fair
		sleep(5);
	}
}