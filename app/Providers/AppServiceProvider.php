<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    	$this->mergeConfigFrom(
    		__DIR__.'/../../config/sms2web.php', 'sms2web'
    	);

		$this->app->singleton('\WMD\WebMessagesDispatcher', function () {

			$wmd = new \WMD\WebMessagesDispatcher();

			// Add WordsCloud dispatcher
			$wmd->registerDispatcher('\WMD\Dispatchers\WordsCloud');

			// Add BotQ dispatcher
			$wmd->registerDispatcher('\WMD\Dispatchers\BotQ');

			// Add Sms dispatcher
			$wmd->registerDispatcher('\WMD\Dispatchers\SmsDispatcher');

    		return $wmd;
    	});

    }

}
