<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Log;
use WMD\Models\Message ;

class ApiController extends BaseController
{
	public function ping()
	{
		return response()->json(array('status'=>'OK'));
	}

	public function stats(/*\Laravel\Lumen\Application $app,*/ \WMD\WebMessagesDispatcher $wmd)
	{
        $stats = array(
            'messagesCount' => 0,
            'routesCount' => $wmd->getRouter()->getRoutesCount()
        );
        return response()->json($stats);
	}

	public function messagePost(Request $request, \WMD\WebMessagesDispatcher $wmd)
	{
		//Log::debug( __METHOD__.' Msg: '. print_r($request->all(), true)  );

		$msg = new Message( $request->all() );

		$dispatched = $wmd->dispatch_message( $msg );

		return response()->json(array(
			'dispatched'=> $dispatched
		));
	}

	public function routeTest( Request $request, \WMD\WebMessagesDispatcher $wmd )
	{
		$input = $request->all();
		$route = $wmd->getRouter()
			->getRoute(
				$input['from_service'],$input['to_service'], $input['from'], $input['to']);
		return response()->json(
			(empty($route) ? false : true)
		);
	}

	public function routeModulesGet(\WMD\WebMessagesDispatcher $wmd)
	{
		return response()->json(
			$wmd->getRouteModules()
		);
	}

	public function routeServicesGet(\WMD\WebMessagesDispatcher $wmd)
	{
		return response()->json(
			$wmd->getRouteServices()
		);
	}

	/**
	 * Routes GET : return all routes
	 * @param \WMD\WebMessagesDispatcher $wmd
	 */
	public function routesGet(\WMD\WebMessagesDispatcher $wmd)
	{
		return response()->json(
			$wmd->getRouter()->getRoutes()
		);
	}

	/**
	 * Route update
	 * @param Request $request
	 * @param \WMD\WebMessagesDispatcher $wmd
	 */
	public function routesPut(Request $request, \WMD\WebMessagesDispatcher $wmd)
	{
		return response()->json(
			$wmd->getRouter()->putRoute( $request->all() )
		);
	}

	/**
	 * @param Request $request
	 * @param \WMD\WebMessagesDispatcher $wmd
	 */
	public function routesPost(Request $request, \WMD\WebMessagesDispatcher $wmd)
	{
		return response()->json(
			$wmd->getRouter()->postRoute( $request->all() )
		);
	}

	public function routesDelete(Request $request, \WMD\WebMessagesDispatcher $wmd)
	{
		return response()->json(
			$wmd->getRouter()->deleteRoute( $request->all() )
		);
	}

}
