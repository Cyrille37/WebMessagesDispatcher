<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class AuthController extends BaseController
{
    public function index()
    {
        return view('auth.index');
    }

    public function login()
    {
    }

    public function logout()
    {
    }

    public function lostpassword()
    {
    }

    public function resetpassword()
    {
    }

}
