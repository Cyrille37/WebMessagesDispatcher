<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Laravel\Lumen\Routing\Controller as BaseController;
use Log;
use SMSEngine\Models\Sms;
use SMSEngine\SmsEngine;
use WMD\Models\Message;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class SmsController extends BaseController
{
	public function get( $phone )
	{
		//Log::debug( __METHOD__.' phone:['.$phone.']' );

		/*
		$sms = Sms
			::where('from','=', $from)
			->where('sent','=', 0)->first();
		*/

		$se = new SmsEngine();
		$sms = $se->popSms( $phone );

		//if( $sms )
		//	Log::debug( __METHOD__.' found sms from:['.$sms->from.'], to:['.$sms->to.'], body:['.$sms->body.']' );

		return response()->json(array('sms'=>$sms));
	}

	/**
	 * Processing Sms2web gateway protocol for pushing or pulling sms
	 *
	 * @see https://git.framasoft.org/Cyrille37/Sms2Web
	 *
	 * @param Request $request
	 * @param \WMD\WebMessagesDispatcher $wmd
	 * @throws BadRequestHttpException
	 * @throws UnauthorizedHttpException
	 * @throws HttpException
	 */
	public function sms2web( Request $request, \WMD\WebMessagesDispatcher $wmd )
	{
		//Log::debug( __METHOD__.'' );

		//
		// Action
		//
		if( ($action = $request->get('action', null)) == null )
		{
			throw new BadRequestHttpException('Invalid action');
		}

		if( $action == 'push')
		{
			$data = $this->validSms2WebRequest( $request, $action );

			// Message for the WMD Dispatcher
			$msg = new Message( [
				'service' => 'sms',
				'from' => $data->from,
				'from_at' => $data->from_at,
				'to' => $data->to,
				'to_at' => $data->to_at,
				'body' => $data->body
			] );

			$dispatched = $wmd->dispatch_message( $msg );

			return response()->json(array(
				'status' => 'OK',
				'dispatched'=> $dispatched
			));
		}
		else if( $action == 'pull')
		{
			$data = $this->validSms2WebRequest( $request, $action );

			$se = new SmsEngine();
			$sms = $se->popSms( $data->from );
			
			//if( $sms )
			//	Log::debug( __METHOD__.' found sms from:['.$sms->from.'], to:['.$sms->to.'], body:['.$sms->body.']' );

			return response()->json(array(
				'status' => 'OK',
				'sms'=>$sms
			));
		}
		else
		{
			throw new BadRequestHttpException('Invalid action');
		}

	}

	/**
	 * Auth - Sms2web gateway protocol.
	 * 
	 * @param Request $request
	 * @param string $forAction
	 * @throws BadRequestHttpException
	 * @throws UnauthorizedHttpException
	 * @throws HttpException
	 */
	protected function validSms2WebRequest( Request $request, $forAction )
	{
		//
		// Channel
		//
		$channels = Config::get('sms2web.channels');

		$channel = $request->get('channel');
		if( empty($channel) || ! isset($channels[$channel]) )
		{
			throw new UnauthorizedHttpException('Invalid channel');
		}

		//
		// Sign
		//
		$secret = $channels[$channel] ;
		$data = json_decode( $request->getContent() );
		if( $data == null )
			throw new BadRequestHttpException('Invalid data');

		if( $forAction == 'push' )
			$sign = sha1( $secret
				. $data->from . $data->from_at
				. $data->to . $data->to_at
				. $data->body
			);
		else if( $forAction == 'pull' )
			$sign = sha1( $secret
				. $data->from
				. $data->to
			);

		if( strtolower($data->sign) != strtolower($sign) )
		{
			throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY);
		}

		return $data ;
	}

}
